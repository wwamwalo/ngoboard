# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
 
Nature.create([{name: 'Agriculture'}, {name: 'Animal Welfare'},{name: 'Culture'},{name: 'Disability'},
  {name: 'Drug and Alcohol Addiction'},{name: 'Education'},{name: 'Energy'},{name: 'Environmental Conservation'},
  {name: 'Health'},{name: 'Peace Building'},
  {name: 'HIV/AIDs awareness/ mitigation'},{name: 'Housing and Settlement'},{name: 'ICT'},{name: 'Microfinance'},
  {name: 'Population and Reproductive Health'},{name: 'Promotion of Good Governance'},
  {name: 'Promotion of Human Rights'},{name: 'Relief/ Disaster Management'},{name: 'Relief of Poverty'},
  {name: 'Road Safety'},{name: 'Sports'},{name: 'Water and Sanitation'},{name: 'Welfare'}])

Target.create([{name: 'Caregivers for vulnerable groups(OVCs, Elderly, PLWAs)'},{name: 'Children in general'},
  {name: 'Children living with HIV and AIDs'}, {name: 'Commercial Sex Workers'},{name: 'Drus and alcohol addicts'},
  {name: 'Elderly People'},{name:'Homeless People'},{name: 'Orphans and vulnerable Children'},
  {name: 'People with disability'},
  {name: 'People living with HIV and AIDS'},{name:'Prisoners and ex-convicts'},{name: 'Poor women'},{name: 'Refugees'},
  {name: 'Society in general'},{name: 'Street Children'},{name: 'The poor in general'},{name: 'Youth'}])
  
Operation.create([{name: 'Acts as Umbrella or network body for NGOc/CBOs' },{name: 'Makes grants to individuals' },
  {name: 'Makes grants to groups or individuals' },{name: 'Makes grants to organizations' },
  {name: 'Provides Training to individuals' },{name: 'Provides training to communities' },
  {name: 'Provides buildings/facilities' },{name: 'Provides services' },{name: 'Provides human resources' },
  {name: 'Provides equipment/ materials' },{name: 'Provides loans to individuals' },
  {name: 'Pays school fees/ provide learning materials for school going children' },{name: 'Provides advocacy/information/advice' },
  {name: 'Provides food aid and other basic necessities to communities/individuals' },{name: 'Undertakes research' }])
  
Expense.create([{sector: 'Agriculture '},{sector: 'Children '},{sector: 'Disability '},{sector: 'Education '},
  {sector: 'Environment '},{sector: 'Gender '},{sector: 'Governance '},
  {sector: 'Health '},{sector: 'HIV/AIDs '},{sector: 'Informal Sector '},{sector: 'Information Communication and Technology '},
  {sector: 'Micro-finance '},{sector: 'Old Age Care '},{sector: 'Peace Building '},
  {sector: 'Population and Reproductive Health '},{sector: 'Refugees '},{sector: 'Relief '},
  {sector: 'Water '},{sector: 'Welfare '},{sector: 'Youth '}])

Income.create([{donor: 'Religious Institution '},{donor: 'Research/ Academic Institution '},{donor: 'Agency of Kenya Government '},
  {donor: 'Embassy/ High Commission '},{donor: 'Foundation/ Trust '},{donor: 'Headquaters of this NGO '},
  {donor: 'United Nations Agency '},{donor: 'Individual Donors '},{donor: 'Foreign Government Agency '},{donor: 'Non Governmental Organization '},
  {donor: 'Director\'s contribution '},{donor: 'Membership Subscription '},{donor: 'Corporate donors (eg Business companies)'},
  {donor: 'NGO\'s self generated Income (eg Consultancy Services, Farming and Business income)'},
  {donor: 'Returns from investments (eg dividends & interest)'}])
  
Asset.create([{item: 'Land'},{item: 'Building'},{item: 'Machinery'},{item: 'Motor Vehicles'},{item: 'Furniture and Fittings'},
  {item: 'Computers'},  {item: 'Printers'},{item: 'Scanners'},{item: 'Photocopiers'},{item: 'Fax Machines'},
  {item: 'Investments securities (eg Shares, bonds)'},{item: 'Reserves'}])
  
Bank.create([{name: 'Cooperative Bank of Kenya'}, {name: 'Chase Bank'}, {name: 'Barclays Bank'},{name: 'Standard Chartered Bank'}])
  
Privilege.create([{name: 'Allowances/Stipends'},{name: 'Housing'},{name: 'Insurance'},{name: 'Medical'},{name: 'Training'}])

Training.create([{name: 'In-house Training'}, {name: 'Professional Training'}])

LocalContribution.create([{name: 'Material'},{name: 'Labour'},{name: 'Financial'}])

GovernmentContribution.create([{name: 'Tax Waiver and VAT Exemption'}])

Collaborate.create([{name: 'NGOs'},{name: 'CBOs'},{name: 'FBOs'},{name: 'Reserach Institutions'},
  {name: 'Academic Institutions'},{name: 'Health Institutions'},{name: 'Government of Kenya Agencies'},
  {name: 'Donor Agencies'}])
  
Election.create([{frequency: 'Annually'}, {frequency: 'Every 2 years'}, {frequency: 'Every 3 years'}, 
  {frequency: 'Every 4 years'}, {frequency: 'Every 5 years'}])
