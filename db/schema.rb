# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150223032516) do

  create_table "admins", force: true do |t|
    t.string   "email",                            default: "", null: false
    t.string   "encrypted_password",               default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role"
    t.string   "names"
    t.boolean  "gender"
    t.integer  "roles_mask"
    t.string   "org_status"
    t.string   "username"
    t.string   "passport"
    t.string   "org_name"
    t.string   "agreement",              limit: 3
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "assets", force: true do |t|
    t.string   "item"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "banks", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "benefits", force: true do |t|
    t.boolean  "local_vol"
    t.boolean  "local_intern"
    t.boolean  "intl_vol"
    t.boolean  "intl_intern"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "finance_id"
    t.integer  "privilege_id"
  end

  create_table "collaborates", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "collaborations", force: true do |t|
    t.integer  "collaborate_id"
    t.boolean  "info_exchange"
    t.boolean  "tech_to"
    t.boolean  "tech_fro"
    t.boolean  "fund_to"
    t.boolean  "fund_fro"
    t.boolean  "equip_to"
    t.boolean  "equip_fro"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "finance_id"
  end

  create_table "contributions", force: true do |t|
    t.integer  "finance_id"
    t.integer  "government_contribution_id"
    t.boolean  "contributed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "donors", force: true do |t|
    t.integer  "name"
    t.integer  "income_id"
    t.integer  "finance_id"
    t.string   "country"
    t.decimal  "amount",     precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "educations", force: true do |t|
    t.string   "name"
    t.string   "certificate"
    t.date     "start"
    t.date     "completion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "official_id"
  end

  create_table "elections", force: true do |t|
    t.string   "frequency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "employees", force: true do |t|
    t.integer  "prev_year_local_kenya"
    t.integer  "prev_year_intl_kenya"
    t.integer  "current_year_local_kenya"
    t.integer  "current_year_intl_kenya"
    t.integer  "this_year_local_add_kenya"
    t.integer  "this_year_intl_add_kenya"
    t.integer  "this_year_local_left_kenya"
    t.integer  "this_year_intl_left_kenya"
    t.integer  "prev_year_local_others"
    t.integer  "prev_year_intl_others"
    t.integer  "current_year_local_others"
    t.integer  "current_year_intl_others"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "finance_id"
    t.integer  "current_year_intl_interns"
    t.integer  "current_year_local_interns"
    t.integer  "prev_year_intl_interns"
    t.integer  "prev_year_local_interns"
  end

  create_table "expenses", force: true do |t|
    t.string   "sector"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "finances", force: true do |t|
    t.boolean  "audited"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "cash_brought_forward",  precision: 10, scale: 2
    t.decimal  "income_subtotal",       precision: 10, scale: 2
    t.decimal  "receipts_total",        precision: 10, scale: 2
    t.decimal  "asset_purchase_kenya",  precision: 10, scale: 2
    t.decimal  "asset_purchase_others", precision: 10, scale: 2
    t.decimal  "project_cost_kenya",    precision: 10, scale: 2
    t.decimal  "project_cost_others",   precision: 10, scale: 2
    t.decimal  "admin_cost_kenya",      precision: 10, scale: 2
    t.decimal  "admin_cost_others",     precision: 10, scale: 2
    t.decimal  "local_staff_kenya",     precision: 10, scale: 2
    t.decimal  "local_staff_others",    precision: 10, scale: 2
    t.decimal  "intl_staff_kenya",      precision: 10, scale: 2
    t.decimal  "intl_staff_others",     precision: 10, scale: 2
    t.decimal  "running_cost_kenya",    precision: 10, scale: 2
    t.decimal  "running_cost_others",   precision: 10, scale: 2
    t.decimal  "total_pay_kenya",       precision: 10, scale: 2
    t.decimal  "total_pay_others",      precision: 10, scale: 2
    t.decimal  "closing_balance",       precision: 10, scale: 2
    t.decimal  "actual_total_balance",  precision: 10, scale: 2
    t.integer  "organization_id"
  end

  create_table "fortunes", force: true do |t|
    t.integer  "finance_id"
    t.integer  "bank_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "branch"
  end

  create_table "government_contributions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "governs", force: true do |t|
    t.integer  "meetings_constitution"
    t.integer  "meetings_last_year"
    t.integer  "meetings_current_year"
    t.date     "last_agm_meeting"
    t.date     "last_election"
    t.integer  "total_officials"
    t.boolean  "lost_assets"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "finance_id"
    t.string   "frequency"
  end

  create_table "incomes", force: true do |t|
    t.string   "donor"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "local_contributions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locals", force: true do |t|
    t.boolean  "contributed"
    t.decimal  "amount",                precision: 10, scale: 2
    t.integer  "finance_id"
    t.integer  "local_contribution_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "natures", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "natures_organizations", force: true do |t|
    t.integer  "organization_id"
    t.integer  "nature_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "occurences", force: true do |t|
    t.integer  "finance_id"
    t.integer  "election_id"
    t.string   "frequency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "officials", force: true do |t|
    t.string   "name"
    t.string   "postal_address"
    t.string   "permanent_address"
    t.string   "residential_address"
    t.string   "previous_name"
    t.string   "county"
    t.string   "location"
    t.string   "sub_location"
    t.string   "telephone"
    t.string   "email"
    t.date     "date_of_birth"
    t.string   "place_of_birth"
    t.string   "nationality_current"
    t.string   "nationality_previous"
    t.string   "passport_number"
    t.string   "pin_number"
    t.string   "place_issue_passport"
    t.string   "occupation"
    t.string   "attainment_place"
    t.string   "attainment_date"
    t.string   "current_employment"
    t.boolean  "acknowledge"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
    t.string   "passport"
    t.string   "designation"
    t.text     "id_upload"
  end

  create_table "operations", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "operations_organizations", force: true do |t|
    t.integer  "organization_id"
    t.integer  "operation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "organizations", force: true do |t|
    t.string   "name"
    t.string   "postal_address"
    t.string   "physical_address"
    t.string   "telephone"
    t.string   "cellphone"
    t.string   "email"
    t.date     "reg_date"
    t.string   "reg_country"
    t.string   "nature"
    t.string   "target"
    t.string   "operation_mode"
    t.date     "financial_year"
    t.string   "operation_country"
    t.string   "planned_country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "reservation_id"
    t.string   "fax"
    t.string   "website"
    t.date     "financial_year_start"
    t.string   "pin_no"
    t.string   "scope"
    t.string   "reg_no"
    t.string   "logo"
    t.text     "minutes"
    t.text     "constitution"
    t.text     "budget"
    t.text     "forward_letter"
    t.text     "notarized_cert"
    t.decimal  "last_income",          precision: 10, scale: 2
  end

  create_table "organizations_targets", force: true do |t|
    t.integer  "organization_id"
    t.integer  "target_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "possessions", force: true do |t|
    t.integer  "finance_id"
    t.integer  "asset_id"
    t.integer  "quantity"
    t.decimal  "value",      precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "privileges", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", force: true do |t|
    t.boolean  "prev_proj_kenya"
    t.boolean  "prev_proj_others"
    t.boolean  "start_proj_kenya"
    t.boolean  "start_proj_others"
    t.boolean  "done_proj_kenya"
    t.boolean  "done_proj_others"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "finance_id"
    t.integer  "expense_id"
  end

  create_table "reservations", force: true do |t|
    t.string   "names"
    t.string   "address"
    t.string   "name1"
    t.string   "name2"
    t.string   "name3"
    t.integer  "user"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "admin_id"
    t.string   "status"
    t.string   "reasons"
    t.integer  "officer"
    t.string   "approved_name"
    t.decimal  "fee",           precision: 10, scale: 2
    t.boolean  "payment"
  end

  create_table "sectors", force: true do |t|
    t.integer  "expense_id"
    t.decimal  "cost_kenya",  precision: 10, scale: 2
    t.decimal  "cost_others", precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "finance_id"
  end

  create_table "static_pages", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "targets", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trained_members", force: true do |t|
    t.integer  "training_id"
    t.integer  "finance_id"
    t.integer  "local_staff"
    t.integer  "intl_staff"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trainings", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "waivers", force: true do |t|
    t.string   "item"
    t.decimal  "amount",     precision: 10, scale: 2
    t.integer  "finance_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
