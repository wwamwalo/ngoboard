class CreateElections < ActiveRecord::Migration
  def change
    create_table :elections do |t|
      t.string :frequency

      t.timestamps
    end
  end
end
