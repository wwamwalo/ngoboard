class ChangeDataTypeForDonorAmount < ActiveRecord::Migration
  def change
    def self.up
      change_table :donors do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 2
      end
    end
  end
end
