class CreatePossessions < ActiveRecord::Migration
  def change
    create_table :possessions do |t|
      t.integer :finance_id
      t.integer :asset_id
      t.integer :quantity
      t.decimal :value

      t.timestamps
    end
  end
end
