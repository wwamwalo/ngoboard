class AddFaxToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :fax, :string
    add_column :organizations, :website, :string
    add_column :organizations, :financial_year_start, :date
  end
end
