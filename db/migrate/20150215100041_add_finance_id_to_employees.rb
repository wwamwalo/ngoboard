class AddFinanceIdToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :finance_id, :integer
  end
end
