class AddBranchToFortunes < ActiveRecord::Migration
  def change
    add_column :fortunes, :branch, :string
  end
end
