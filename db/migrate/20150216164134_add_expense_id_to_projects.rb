class AddExpenseIdToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :expense_id, :integer
  end
end
