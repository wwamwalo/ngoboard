class ChangeIncomeInOrganizations < ActiveRecord::Migration
   def change
    remove_column :organizations, :last_income, :string
    add_column :organizations, :last_income, :decimal, :precision => 10, :scale => 2
  end
end
