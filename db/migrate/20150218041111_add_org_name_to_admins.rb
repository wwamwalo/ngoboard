class AddOrgNameToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :org_name, :string
  end
end
