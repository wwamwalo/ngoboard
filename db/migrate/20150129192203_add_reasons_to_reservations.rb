class AddReasonsToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :reasons, :string
  end
end
