class AddFieldToFinances < ActiveRecord::Migration
  def change
    add_column :finances,  :prev_year_local_kenya, :integer
    add_column :finances, :prev_year_intl_kenya, :integer
    add_column :finances, :current_year_local_kenya, :integer
    add_column :finances, :current_year_intl_kenya, :integer
    add_column :finances, :this_year_local_add_kenya, :integer
    add_column :finances, :this_year_intl_add_kenya, :integer
    add_column :finances, :this_year_local_left_kenya, :integer
    add_column :finances, :this_year_intl_left_kenya, :integer
    add_column :finances, :prev_year_local_others, :integer
    add_column :finances, :prev_year_intl_others, :integer
    add_column :finances, :current_year_local_others, :integer
    add_column :finances, :current_year_intl_others, :integer
  end
end
