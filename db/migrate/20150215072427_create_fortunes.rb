class CreateFortunes < ActiveRecord::Migration
  def change
    create_table :fortunes do |t|
      t.integer :finance_id
      t.integer :bank_id

      t.timestamps
    end
  end
end
