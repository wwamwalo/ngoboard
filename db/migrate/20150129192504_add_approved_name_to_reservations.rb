class AddApprovedNameToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :approved_name, :string
  end
end
