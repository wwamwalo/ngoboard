class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :postal_address
      t.string :physical_address
      t.string :telephone
      t.string :cellphone
      t.string :email
      t.date :reg_date
      t.string :reg_country
      t.string :nature
      t.string :target
      t.string :operation_mode
      t.string :last_income
      t.string :financial_year
      t.string :operation_country
      t.string :planned_country 
      
      t.timestamps
    end
  end
end
