class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.integer :finance_id
      t.integer :government_contribution_id
      t.boolean :contributed

      t.timestamps
    end
  end
end
