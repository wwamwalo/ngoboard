class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.boolean :prev_proj_kenya
      t.boolean :prev_proj_others
      t.boolean :start_proj_kenya
      t.boolean :start_proj_others
      t.boolean :done_proj_kenya
      t.boolean :done_proj_others

      t.timestamps
    end
  end
end
