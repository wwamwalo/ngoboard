class RemovePrevYearLocalKenyaFromFinances < ActiveRecord::Migration
  def change
    remove_column :finances,  :prev_year_local_kenya, :integer
    remove_column :finances, :prev_year_intl_kenya, :integer
    remove_column :finances, :current_year_local_kenya, :integer
    remove_column :finances, :current_year_intl_kenya, :integer
    remove_column :finances, :this_year_local_add_kenya, :integer
    remove_column :finances, :this_year_intl_add_kenya, :integer
    remove_column :finances, :this_year_local_left_kenya, :integer
    remove_column :finances, :this_year_intl_left_kenya, :integer
    remove_column :finances, :prev_year_local_others, :integer
    remove_column :finances, :prev_year_intl_others, :integer
    remove_column :finances, :current_year_local_others, :integer
    remove_column :finances, :current_year_intl_others, :integer
    remove_column :banks, :branch
  end
end
