class AddFinanceIdToGoverns < ActiveRecord::Migration
  def change
    add_column :governs, :finance_id, :integer
  end
end
