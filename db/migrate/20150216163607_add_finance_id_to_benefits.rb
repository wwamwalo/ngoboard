class AddFinanceIdToBenefits < ActiveRecord::Migration
  def change
    add_column :benefits, :finance_id, :integer
    add_column :benefits, :privilege_id, :integer
  end
end
