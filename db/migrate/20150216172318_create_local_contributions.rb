class CreateLocalContributions < ActiveRecord::Migration
  def change
    create_table :local_contributions do |t|
      t.string :name

      t.timestamps
    end
  end
end
