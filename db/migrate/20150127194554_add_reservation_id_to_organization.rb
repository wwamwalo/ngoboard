class AddReservationIdToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :reservation_id, :integer
  end
end
