class CreateOfficials < ActiveRecord::Migration
  def change
    create_table :officials do |t|
      t.string :name
      t.string :postal_address
      t.string :permanent_address
      t.string :residential_address 
      t.string :previous_name
      t.string :county
      t.string :location
      t.string :sub_location
      t.string :telephone
      t.string :email
      t.date :date_of_birth
      t.string :place_of_birth
      t.string :nationality_current
      t.string :nationality_previous
      t.string :passport_number
      t.string :pin_number
      t.string :place_issue_passport
      t.string :occupation
      t.string :attainment_place
      t.string :attainment_date
      t.string :current_employment
      t.boolean :acknowledge
      
      t.timestamps
    end
  end
end
