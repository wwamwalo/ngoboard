class AddCurrentYearIntlInternsToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :current_year_intl_interns, :integer
    add_column :employees, :current_year_local_interns, :integer
    add_column :employees, :prev_year_intl_interns, :integer
    add_column :employees, :prev_year_local_interns, :integer
  end
end
