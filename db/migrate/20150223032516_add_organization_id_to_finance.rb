class AddOrganizationIdToFinance < ActiveRecord::Migration
  def change
    add_column :finances, :organization_id, :integer
  end
end
