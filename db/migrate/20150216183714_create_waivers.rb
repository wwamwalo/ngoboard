class CreateWaivers < ActiveRecord::Migration
  def change
    create_table :waivers do |t|
      t.string :item
      t.decimal :amount
      t.integer :finance_id

      t.timestamps
    end
  end
end
