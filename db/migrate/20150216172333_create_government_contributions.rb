class CreateGovernmentContributions < ActiveRecord::Migration
  def change
    create_table :government_contributions do |t|
      t.string :name

      t.timestamps
    end
  end
end
