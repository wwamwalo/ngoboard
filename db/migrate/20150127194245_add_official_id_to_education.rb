class AddOfficialIdToEducation < ActiveRecord::Migration
  def change
    add_column :educations, :official_id, :integer
  end
end
