class ChangeDataTypeForTableAmount < ActiveRecord::Migration
  def self.up
    change_table :locals do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 2
    end
    
    
    change_table :possessions do |t|
        t.change :value, :decimal, :precision => 10, :scale => 2
    end
    
    change_table :reservations do |t|
        t.change :fee, :decimal, :precision => 10, :scale => 2
    end
    
    change_table :sectors do |t|
        t.change :cost_kenya, :decimal, :precision => 10, :scale => 2
        t.change :cost_others, :decimal, :precision => 10, :scale => 2
    end
    
    change_table :waivers do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 2
    end
  end

  def self.down
    change_table :locals do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 0
    end
        
    change_table :possessions do |t|
        t.change :value, :decimal, :precision => 10, :scale => 0
    end
    
    change_table :reservations do |t|
        t.change :fee, :decimal, :precision => 10, :scale => 0
    end
    
    change_table :sectors do |t|
        t.change :cost_kenya, :decimal, :precision => 10, :scale => 0
        t.change :cost_others, :decimal, :precision => 10, :scale => 0
    end
    
    change_table :waivers do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 0
    end
  end
end
