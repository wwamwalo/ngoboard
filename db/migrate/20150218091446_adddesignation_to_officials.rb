class AdddesignationToOfficials < ActiveRecord::Migration
  def change
    add_column :officials, :designation, :string
    add_column :officials, :id_upload, :string
  end
end
