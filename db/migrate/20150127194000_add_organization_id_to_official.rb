class AddOrganizationIdToOfficial < ActiveRecord::Migration
  def change
    add_column :officials, :organization_id, :integer
  end
end
