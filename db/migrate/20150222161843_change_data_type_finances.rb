class ChangeDataTypeFinances < ActiveRecord::Migration
  def change

    remove_column :finances, :cash_brought_forward, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :income_subtotal, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :receipts_total, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :asset_purchase_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :asset_purchase_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :project_cost_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :project_cost_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :admin_cost_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :admin_cost_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :local_staff_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :local_staff_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :intl_staff_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :intl_staff_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :running_cost_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :running_cost_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :total_pay_kenya, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :total_pay_others, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :closing_balance, :decimal, :precision => 10, :scale => 0
    remove_column :finances, :actual_total_balance, :decimal, :precision => 10, :scale => 0
    

    add_column :finances, :cash_brought_forward, :decimal, :precision => 10, :scale => 2
    add_column :finances, :income_subtotal, :decimal, :precision => 10, :scale => 2
    add_column :finances, :receipts_total, :decimal, :precision => 10, :scale => 2
    add_column :finances, :asset_purchase_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :asset_purchase_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :project_cost_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :project_cost_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :admin_cost_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :admin_cost_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :local_staff_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :local_staff_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :intl_staff_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :intl_staff_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :running_cost_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :running_cost_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :total_pay_kenya, :decimal, :precision => 10, :scale => 2
    add_column :finances, :total_pay_others, :decimal, :precision => 10, :scale => 2
    add_column :finances, :closing_balance, :decimal, :precision => 10, :scale => 2
    add_column :finances, :actual_total_balance, :decimal, :precision => 10, :scale => 2
  end
end
