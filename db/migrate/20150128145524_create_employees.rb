class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.integer :prev_year_local_kenya
      t.integer :prev_year_intl_kenya
      t.integer :current_year_local_kenya
      t.integer :current_year_intl_kenya
      t.integer :this_year_local_add_kenya
      t.integer :this_year_intl_add_kenya
      t.integer :this_year_local_left_kenya
      t.integer :this_year_intl_left_kenya
      t.integer :prev_year_local_others
      t.integer :prev_year_intl_others
      t.integer :current_year_local_others
      t.integer :current_year_intl_others
      
      t.timestamps
    end
  end
end
