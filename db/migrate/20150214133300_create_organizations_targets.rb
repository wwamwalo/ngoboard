class CreateOrganizationsTargets < ActiveRecord::Migration
  def change
    create_table :organizations_targets do |t|
      t.integer :organization_id
      t.integer :target_id

      t.timestamps
    end
  end
end
