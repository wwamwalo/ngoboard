class CreateGoverns < ActiveRecord::Migration
  def change
    create_table :governs do |t|
      t.integer :meetings_constitution
      t.integer :meetings_last_year
      t.integer :meetings_current_year
      t.date :last_agm_meeting
      t.date :last_election
      t.integer :total_officials
      t.boolean :lost_assets

      t.timestamps
    end
  end
end
