class AddAdminIdToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :admin_id, :integer
  end
end
