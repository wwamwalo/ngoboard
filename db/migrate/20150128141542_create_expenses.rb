class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :sector

      t.timestamps
    end
  end
end
