class CreateBenefits < ActiveRecord::Migration
  def change
    create_table :benefits do |t|
      t.boolean :local_vol
      t.boolean :local_intern
      t.boolean :intl_vol
      t.boolean :intl_intern

      t.timestamps
    end
  end
end
