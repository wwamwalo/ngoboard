class AddAgreementToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :agreement, :string, :limit=> 3
  end
end
