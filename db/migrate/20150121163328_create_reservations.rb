class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :names
      t.string :address
      t.string :name1
      t.string :name2
      t.string :name3
      t.integer :user

      t.timestamps
    end
  end
end
