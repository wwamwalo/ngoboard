class AddPinNoToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :pin_no, :string
    add_column :organizations, :scope, :string
    add_column :organizations, :reg_no, :string
  end
end
