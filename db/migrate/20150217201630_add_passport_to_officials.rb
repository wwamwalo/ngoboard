class AddPassportToOfficials < ActiveRecord::Migration
  def change
    add_column :officials, :passport, :string
  end
end
