class CreateOperationsOrganizations < ActiveRecord::Migration
  def change
    create_table :operations_organizations do |t|
      t.integer :organization_id
      t.integer :operation_id

      t.timestamps
    end
  end
end
