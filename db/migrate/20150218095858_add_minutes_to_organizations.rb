class AddMinutesToOrganizations < ActiveRecord::Migration
  def change
    add_column :organizations, :minutes, :text
    add_column :organizations, :constitution, :text
    add_column :organizations, :budget, :text
    add_column :organizations, :forward_letter, :text
    add_column :organizations, :notarized_cert, :text
  end
end
