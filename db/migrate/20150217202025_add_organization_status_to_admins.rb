class AddOrganizationStatusToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :org_status, :string
    add_column :admins, :username, :string
    add_column :admins, :passport, :string
  end
end
