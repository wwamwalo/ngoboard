class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :name
      t.string :certificate
      t.date :start
      t.date :completion

      t.timestamps
    end
  end
end
