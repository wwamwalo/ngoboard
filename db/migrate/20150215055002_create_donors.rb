class CreateDonors < ActiveRecord::Migration
  def change
    create_table :donors do |t|
      t.integer :name
      t.integer :income_id
      t.integer :finance_id
      t.string :country
      t.decimal :amount

      t.timestamps
    end
  end
end
