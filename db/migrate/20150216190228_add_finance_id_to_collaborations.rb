class AddFinanceIdToCollaborations < ActiveRecord::Migration
  def change
    add_column :collaborations, :finance_id, :integer
  end
end
