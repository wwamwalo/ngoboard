class AddFinanceIdToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :finance_id, :integer
  end
end
