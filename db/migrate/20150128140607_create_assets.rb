class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :item

      t.timestamps
    end
  end
end
