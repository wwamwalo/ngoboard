class AddNamesToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :names, :string
    add_column :admins, :gender, :boolean
  end
end
