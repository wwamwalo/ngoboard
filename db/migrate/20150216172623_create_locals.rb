class CreateLocals < ActiveRecord::Migration
  def change
    create_table :locals do |t|
      t.boolean :contributed
      t.decimal :amount
      t.integer :finance_id
      t.integer :local_contribution_id

      t.timestamps
    end
  end
end
