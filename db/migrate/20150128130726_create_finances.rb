class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.decimal :cash_brought_forward
      t.decimal :income_subtotal
      t.decimal :receipts_total
      t.decimal :asset_purchase_kenya
      t.decimal :asset_purchase_others
      t.decimal :project_cost_kenya
      t.decimal :project_cost_others
      t.decimal :admin_cost_kenya
      t.decimal :admin_cost_others
      t.decimal :local_staff_kenya
      t.decimal :local_staff_others
      t.decimal :intl_staff_kenya
      t.decimal :intl_staff_others
      t.decimal :running_cost_kenya
      t.decimal :running_cost_others
      t.decimal :total_pay_kenya
      t.decimal :total_pay_others
      t.decimal :closing_balance
      t.decimal :actual_total_balance
      t.boolean :audited

      t.timestamps
    end
  end
end
