class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.string :donor

      t.timestamps
    end
  end
end
