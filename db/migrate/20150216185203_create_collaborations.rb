class CreateCollaborations < ActiveRecord::Migration
  def change
    create_table :collaborations do |t|
      t.integer :collaborate_id
      t.boolean :info_exchange
      t.boolean :tech_to
      t.boolean :tech_fro
      t.boolean :fund_to
      t.boolean :fund_fro
      t.boolean :equip_to
      t.boolean :equip_fro

      t.timestamps
    end
  end
end
