class CreateTrainedMembers < ActiveRecord::Migration
  def change
    create_table :trained_members do |t|
      t.integer :training_id
      t.integer :finance_id
      t.integer :local_staff
      t.integer :intl_staff

      t.timestamps
    end
  end
end
