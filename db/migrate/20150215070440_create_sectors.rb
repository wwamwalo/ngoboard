class CreateSectors < ActiveRecord::Migration
  def change
    create_table :sectors do |t|
      t.integer :expense_id
      t.decimal :cost_kenya
      t.decimal :cost_others

      t.timestamps
    end
  end
end
