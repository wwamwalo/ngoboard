class AddOfficerToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :officer, :integer
  end
end
