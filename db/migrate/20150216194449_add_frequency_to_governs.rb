class AddFrequencyToGoverns < ActiveRecord::Migration
  def change
    add_column :governs, :frequency, :string
  end
end
