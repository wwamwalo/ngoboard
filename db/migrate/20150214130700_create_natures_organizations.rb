class CreateNaturesOrganizations < ActiveRecord::Migration
  def change
    create_table :natures_organizations do |t|
      t.integer :organization_id
      t.integer :nature_id

      t.timestamps
    end
  end
end
