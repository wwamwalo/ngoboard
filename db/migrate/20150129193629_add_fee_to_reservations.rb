class AddFeeToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :fee, :decimal
  end
end
