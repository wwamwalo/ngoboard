class CreateOccurences < ActiveRecord::Migration
  def change
    create_table :occurences do |t|
      t.integer :finance_id
      t.integer :election_id
      t.string :frequency

      t.timestamps
    end
  end
end
