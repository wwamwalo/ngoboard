class AddFinanceIdToSectors < ActiveRecord::Migration
  def change
    add_column :sectors, :finance_id, :integer
  end
end
