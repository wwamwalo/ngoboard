class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.string :currency
      t.decimal :amount

      t.timestamps
    end
  end
end
