class ChangeDataTypeForLocalAmount < ActiveRecord::Migration
  def self.up
    change_table :donors do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 2
    end
  end

  def self.down
    change_table :donors do |t|
        t.change :amount, :decimal, :precision => 10, :scale => 0
    end
  end
end
