class OfficialsController < ApplicationController
  skip_authorization_check 
  load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @official = Official.new
    3.times{@official.educations.build}
  end

  def index
    @officials = Official.all
  end
  
  def create
    @official =  Official.new(official_params)
    if @official.save
      flash[:success] = "you have added a new official"
      Notifier.official(current_admin).deliver # sends the email
      mail = Notifier.official(current_admin)  # => a Mail::Message object
      mail.deliver                    # sends the email
      redirect_to @official
    else
      flash[:danger] = "Official Not added. Please try again"
      render 'new'
    end
  end

  def edit
    @official = Official.find(params[:id])
    3.times{@official.educations.build}
  end
  
  def update
    @official = Official.find(params[:id])
    respond_to do |format|
      if @official.update_attributes(official_params)
        format.html { redirect_to(@official, :success => 'Official was successfully updated') }
        format.json { respond_with_bip(@official) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@official) }
      end
    end
  end
  
  def show
    @official = Official.find(params[:id])
  end

  def destroy
    Official.find(params[:id]).destroy
    flash[:success] = "Official deleted!!"
    redirect_to officials_url
  end
  
  private
  def official_params
    params.require(:official).permit(:name, :postal_address, :permanent_address, :residential_address,:id_upload,
    :previous_name, :county, :location,:sub_location, :telephone,:email, :date_of_birth, :place_of_birth, 
    :nationality_current, :nationality_previous, :passport_number,:pin_number,:place_issue_passport, :occupation,
    :attainment_place, :attainment_date, :current_employment, :acknowledge, :organization_id, :passport,:designation,
    educations_attributes:[:id,:name,:certificate,:start,:completion, :_destroy])
  end
end
