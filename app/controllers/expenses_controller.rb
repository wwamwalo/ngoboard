#expenses details for finance
class ExpensesController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @expense = Expense.new
  end
  
  def create
    @expense =  Expense.new(expense_params)
    if @expense.save
      flash[:success] = "Your Expense group was succesfully added"
      redirect_to newexpense_url
    else
      flash[:error] = "expense Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def expense_params
    params.require(:expense).permit(:sector)
  end
end
