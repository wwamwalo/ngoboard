class EmployeesController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @employee = Employee.new
  end

  def index
    @employees = Employee.all
  end
  
  def create
    @employee =  Employee.new(employee_params)
    if @employee.save
      flash[:success] = "Your Personnel data was succesfully added"
      #Notifier.employee(current_admin).deliver # sends the email
      #mail = Notifier.employee(current_admin)  # => a Mail::Message object
      #mail.deliver    # sends the email
      redirect_to @employee
    else
      flash[:error] = "Employee Not added. Please try again"
      render 'new'
    end
  end
  
  def edit
    @employee = Employee.find(params[:id])
  end
  
  def update
    @employee = Employee.find(params[:id])
    respond_to do |format|
      if @employee.update_attributes(employee_params)
        format.html { redirect_to(@employee, :success => 'Employee was successfully updated') }
        format.json { respond_with_bip(@employee) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@employee) }
      end
    end
  end
  
  def show
    @employee = Employee.find(params[:id])
  end
  
  def destroy
    Employee.find(params[:id]).destroy
    flash[:success] = "Employee deleted!!"
    redirect_to employees_url
  end
  
  private
  def employee_params
    params.require(:employee).permit(:prev_year_local_kenya,:prev_year_intl_kenya, :this_year_local_add_kenya, 
    :this_year_intl_add_kenya,:this_year_local_left_kenya,:this_year_intl_left_kenya,:current_year_local_kenya, 
    :current_year_intl_kenya,:prev_year_local_others,:prev_year_intl_others,:current_year_local_others, 
    :current_year_intl_others, :current_year_intl_interns, :current_year_local_interns,:prev_year_intl_interns, 
    :prev_year_local_interns)
  end
end
