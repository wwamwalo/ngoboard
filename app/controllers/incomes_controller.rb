#capture donor details for finance
class IncomesController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @income = Income.new
  end
  
  def create
    @income =  Income.new(income_params)
    if @income.save
      flash[:success] = "Your Income group was succesfully added"
      redirect_to newincome_url
    else
      flash[:error] = "income Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def income_params
    params.require(:income).permit(:donor)
  end
end
