class OrganizationsController < ApplicationController
  skip_authorization_check 
  load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @organization = Organization.new
    @nature = Nature.all
    @target = Target.all
    @operation = Operation.all
    @official = Official.all
  end
  
  def index
    @organizations = Organization.all
  end
  
  def create
    @organization =  Organization.new(organization_params)
    if @organization.save
      flash[:success] = "Organization has been successfully registered"
      #Notifier.organization(current_admin).deliver # sends the email
      #mail = Notifier.organization(current_admin)  # => a Mail::Message object
      #mail.deliver                    # sends the email
      redirect_to @organization
    else
      flash[:danger] = "Organization Not added. Please try again"
      render 'new'
    end
  end

  def edit
    @organization = Organization.find(params[:id])
  end
  
  def update
    @organization = Organization.find(params[:id])
    respond_to do |format|
      if @organization.update_attributes(organization_params)
        format.html { redirect_to(@organization, :success => 'Organization has been successfully updated') }
        format.json { respond_with_bip(@organization) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@organization) }
      end
    end
  end
  
  def show
    @organization = Organization.find(params[:id])
    @nature = Nature.all
    @target = Target.all
    @operation = Operation.all
  end

  def destroy
    Organization.find(params[:id]).destroy
    flash[:success] = "Organization deleted!!"
    redirect_to organizations_url
  end
  
  private
  def organization_params
    params.require(:organization).permit(:name, :postal_address, :physical_address, :fax, :website, :financial_year_start,
    :telephone, :cellphone, :email, :reg_date, :reg_country, :nature, :target, :operation_mode, :pin_no, :scope, :reg_no,
    :last_income, :financial_year, :operation_country, :planned_country, :logo, :constitution, :minutes, :budget,
    :forward_letter, :notarized_cert, {:nature_ids =>[]}, {:target_ids => []}, {:operation_ids => []})
  end
end
