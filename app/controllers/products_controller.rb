class ProductsController < ApplicationController
  skip_authorization_check 
  before_filter :authenticate_admin!
  respond_to :html, :json

  def new
    @product = Product.new
  end

  def index
    @products = Product.all
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      flash[:success] = "you have saved a new product"
      #Notifier.welcome(current_admin).deliver # sends the email
      #mail = Notifier.welcome(current_admin)  # => a Mail::Message object
      #mail.deliver                    # sends the email
      redirect_to @product
    else
      flash[:danger] = "product Not added. Please try again"
      render 'new'
    end
  end

  def show
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    respond_to do |format|
      if @product.update_attributes(product_params)
        format.html { redirect_to(@product, :success => 'Product was successfully updated') }
        format.json { respond_with_bip(@product) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@product) }
      end
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def destroy
    Product.find(params[:id]).destroy
    flash[:success] = "Product permanently deleted"
    redirect_to products_url
  end

  private
  def product_params
    params.require(:product).permit(:name, :description, :currency, :amount)
  end
end
