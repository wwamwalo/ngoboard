#for finance
class BanksController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @bank = Bank.new
  end
  
  def create
    @bank =  Bank.new(bank_params)
    if @bank.save
      flash[:success] = "Your Bank group was succesfully added"
      redirect_to newbank_url
    else
      flash[:error] = "bank Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def bank_params
    params.require(:bank).permit(:name,:branch)
  end
end
