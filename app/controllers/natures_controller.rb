class NaturesController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @nature = Nature.new
  end
  
  def create
    @nature =  Nature.new(nature_params)
    if @nature.save
      flash[:success] = "Your Nature group was succesfully added"
      redirect_to newnature_url
    else
      flash[:error] = "nature Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def nature_params
    params.require(:nature).permit(:name)
  end
end
