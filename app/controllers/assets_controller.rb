#ngo assets for finance
class AssetsController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @asset = Asset.new
  end
  
  def create
    @asset =  Asset.new(asset_params)
    if @asset.save
      flash[:success] = "Your Asset group was succesfully added"
      redirect_to newasset_url
    else
      flash[:error] = "asset Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def asset_params
    params.require(:asset).permit(:item)
  end
end
