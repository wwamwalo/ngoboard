class EducationsController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @education = Education.new
  end

  def index
    @educations = Education.all
  end
  
  def create
    @education =  Education.new(education_params)
    if @education.save
      flash[:success] = "Your entry was succesfully added"
      Notifier.education(current_admin).deliver # sends the email
      mail = Notifier.education(current_admin)  # => a Mail::Message object
      mail.deliver    # sends the email
      redirect_to @education
    else
      flash[:error] = "Education Not added. Please try again"
      render 'new'
    end
  end
  
  def edit
    @education = Education.find(params[:id])
  end
  
  def update
    @education = Education.find(params[:id])
    respond_to do |format|
      if @education.update_attributes(education_params)
        format.html { redirect_to(@education, :success => 'Education was successfully updated') }
        format.json { respond_with_bip(@education) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@education) }
      end
    end
  end
  
  def show
    @education = Education.find(params[:id])
  end
  
  def destroy
    Education.find(params[:id]).destroy
    flash[:success] = "Education deleted!!"
    redirect_to educations_url
  end
  
  private
  def education_params
    params.require(:education).permit(:name, :certificate, :start, :completion)
  end
end
