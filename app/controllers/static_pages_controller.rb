class StaticPagesController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  def home
  end
  
  def help
  end
  
  def contact
  end
  
  def sitemap
  end
  
  def dashboard
  end
  
  def error
  end
  
  def register
    
  end
  
end
