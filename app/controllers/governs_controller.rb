class GovernsController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @govern = Govern.new
  end

  def index
    @governs = Govern.all
  end
  
  def create
    @govern =  Govern.new(govern_params)
    if @govern.save
      flash[:success] = "Your entry was succesfully added"
      #Notifier.govern(current_admin).deliver # sends the email
      #mail = Notifier.govern(current_admin)  # => a Mail::Message object
      #mail.deliver    # sends the email
      redirect_to @govern
    else
      flash[:error] = "Govern Not added. Please try again"
      render 'new'
    end
  end
  
  def edit
    @govern = Govern.find(params[:id])
  end
  
  def update
    @govern = Govern.find(params[:id])
    respond_to do |format|
      if @govern.update_attributes(govern_params)
        format.html { redirect_to(@govern, :success => 'Govern was successfully updated') }
        format.json { respond_with_bip(@govern) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@govern) }
      end
    end
  end
  
  def show
    @govern = Govern.find(params[:id])
  end
  
  def destroy
    Govern.find(params[:id]).destroy
    flash[:success] = "Govern deleted!!"
    redirect_to governs_url
  end
  
  private
  def govern_params
    params.require(:govern).permit(:meetings_constitution, :meetings_last_year,:meetings_current_year,
    :last_agm_meeting,:last_election,:total_officials,:lost_assets)
  end
end
