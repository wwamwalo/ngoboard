class FinancesController < ApplicationController
  skip_authorization_check 
  load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @finance = Finance.new
    @elections = Election.all
    @collaborates = Collaborate.all
    @government_contributions = GovernmentContribution.all
    @local_contributions = LocalContribution.all
    @expenses = Expense.all
    @trainings = Training.all
    @privileges = Privilege.all
    @finance.possessions.build
    @finance.donors.build
    @finance.sectors.build
    @finance.fortunes.build
    @finance.employee = Employee.new
    @finance.projects.build
    @finance.benefits.build
    @finance.trained_members.build
    @finance.locals.build
    @finance.contributions.build
    @finance.collaborations.build
    @finance.govern = Govern.new
    @finance.occurences.build
  end

  def index
    @finances = Finance.all
  end
  
  def create
    @finance =  Finance.new(finance_params)
    if @finance.save
      flash[:success] = "Financial returns completed successfully"
      #Notifier.finance(current_admin).deliver # sends the email
      #mail = Notifier.finance(current_admin)  # => a Mail::Message object
      #mail.deliver    # sends the email
      redirect_to @finance
    else
      flash[:danger] = "Error submitting financial return. Please try again"
      render 'new'
    end
  end
  
  def edit
    @finance = Finance.find(params[:id])
  end
  
  def update
    @finance = Finance.find(params[:id])
    respond_to do |format|
      if @finance.update_attributes(finance_params)
        format.html { redirect_to(@finance, :success => 'Finance was successfully updated') }
        format.json { respond_with_bip(@finance) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@finance) }
      end
    end
  end
  
  def show
    @finance = Finance.find(params[:id])
  end
  
  def destroy
    Finance.find(params[:id]).destroy
    flash[:success] = "Finance deleted!!"
    redirect_to finances_url
  end
  
  private
  def finance_params
    params.require(:finance).permit(:cash_brought_forward,:income_subtotal,:receipts_total,:asset_purchase_kenya,:asset_purchase_others,
    :project_cost_kenya,:project_cost_others,:admin_cost_kenya,:admin_cost_others,:local_staff_kenya,
    :local_staff_others,:intl_staff_kenya,:intl_staff_others,:running_cost_kenya,:running_cost_others,
    :total_pay_kenya,:total_pay_others,:closing_balance,:actual_total_balance,:audited, 
    possessions_attributes: [:id, :finance_id, :asset_id, :quantity, :value, :_destroy ],
    donors_attributes: [:id, :finance_id, :income_id, :name, :country, :amount, :_destroy ],
    sectors_attributes: [:id, :finance_id, :expense_id, :cost_kenya, :cost_others, :_destroy ],
    fortunes_attributes: [:id, :finance_id, :bank_id, :branch, :_destroy],
    employee_attributes: [:id, :finance_id, :prev_year_local_kenya,:prev_year_intl_kenya, :this_year_local_add_kenya, 
    :this_year_intl_add_kenya,:this_year_local_left_kenya,:this_year_intl_left_kenya,:current_year_local_kenya, 
    :current_year_intl_kenya,:prev_year_local_others,:prev_year_intl_others,:current_year_local_others, 
    :current_year_intl_others, :current_year_intl_interns, :current_year_local_interns,:prev_year_intl_interns, 
    :prev_year_local_interns, :_destroy],
    projects_attributes: [:id, :finance_id, :expense_id, :operation_id, :prev_proj_kenya, :prev_proj_others,
      :start_proj_kenya, :start_proj_kenya, :done_proj_kenya,:done_proj_kenya,:_destroy],
    benefits_attributes: [:id, :finance_id, :privilege_id, :local_vol, :local_intern, :intl_vol, :intl_intern, :_destroy],
    trained_members_attributes: [:id, :finance_id, :training_id, :local_staff, :intl_staff, :_destroy],
    locals_attributes: [:id, :finance_id, :local_contribution_id, :contributed, :amount, :_destroy ],
    contributions_attributes: [:id, :finance_id, :government_contribution_id, :contributed, :_destroy ],
    waivers_attributes: [:id, :finance_id, :item, :amount, :_destroy ],
    collaborations_attributes: [:id, :finance_id, :collaborate_id, :info_exchange, :tech_to, :tech_fro,
      :fund_to, :fund_fro, :equip_to, :equip_fro, :_destroy ],
    govern_attributes: [:id, :finance_id, :meetings_constitution, :meetings_last_year,:meetings_current_year,
    :last_agm_meeting,:last_election,:total_officials,:lost_assets, :_destroy ],
    occurences_attributes: [:id, :finance_id, :election_id, :frequency, :_destroy ],)
  end
end
