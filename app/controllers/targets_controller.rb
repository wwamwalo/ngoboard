#Who will the ngo help
class TargetsController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @target = Target.new
  end
  
  def create
    @target =  Target.new(target_params)
    if @target.save
      flash[:success] = "Your target group was succesfully added"
      redirect_to newtarget_url
    else
      flash[:error] = "Target Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def target_params
    params.require(:target).permit(:name)
  end
end
