class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  check_authorization :unless => :devise_controller?#use can can to authorize all resources
  before_filter :configure_permitted_parameters, if: :devise_controller?
  alias_method :current_user, :current_admin #assume name for cancan purposes 

  #handle the forbidden issue caused by cancan
  before_filter do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end
  
  protected
  def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:names, :email, :password, :password_confirmation,:role, 
        :gender,:username, :org_status, :org_name, :passport, :agreement)}
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:names, :email, :password, :password_confirmation,
        :role, :gender, :username, :org_status, :org_name, :passport, :agreement)}
  end
  
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to error_url, :alert => exception.message
  end
  
end
