#How organization operates
class OperationsController < ApplicationController
  skip_authorization_check 
  #load_and_authorize_resource
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @operation = Operation.new
  end
  
  def create
    @operation =  Operation.new(operation_params)
    if @operation.save
      flash[:success] = "Your operation group was succesfully added"
      redirect_to newoperation_url
    else
      flash[:error] = "Operation Not added. Please try again"
      render 'new'
    end
  end
  
  private
  def operation_params
    params.require(:operation).permit(:name)
  end
end
