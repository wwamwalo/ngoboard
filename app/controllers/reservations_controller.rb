class ReservationsController < ApplicationController
  #load_and_authorize_resource
  skip_authorization_check 
  before_filter :authenticate_admin!
  respond_to :html, :json
  
  def new
    @reservation = Reservation.new
  end

  def index
    @reservations = Reservation.all
    @organizations = Organization.all
  end
  
  def create
    @reservation =  Reservation.new(reservation_params)
    if @reservation.save
      flash[:success] = "you have made a new reservation"
      #Notifier.welcome(current_admin).deliver # sends the email
      #mail = Notifier.welcome(current_admin)  # => a Mail::Message object
      #mail.deliver                    # sends the email
      redirect_to @reservation
    else
      flash[:danger] = "Reservation Not added. Please try again"
      render 'new'
    end
  end

  def edit
    @reservation = Reservation.find(params[:id])
  end
  
  def update
    @reservation = Reservation.find(params[:id])
    respond_to do |format|
      if @reservation.update_attributes(reservation_params)
        format.html { redirect_to(@reservation, :success => 'Reservation was successfully updated') }
        format.json { respond_with_bip(@reservation) }
      else
        format.html { render :action => "edit" }
        format.json { respond_with_bip(@reservation) }
      end
    end
  end
  
  def show
    @reservation = Reservation.find(params[:id])
    @reservation_new = Reservation.find(params[:id])
    @organization = Organization.all
  end

  def destroy
    Reservation.find(params[:id]).destroy
    flash[:success] = "Reservation permanently deleted"
    redirect_to reservations_url
  end
  
  private
  def reservation_params
    params.require(:reservation).permit(:names, :address, :name1, :name2, :name3, :user, :admin_id, :status,
    :reasons,:officer, :approved_name, :fee, :payment)
  end
end
