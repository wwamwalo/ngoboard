class GovernmentContribution < ActiveRecord::Base
  has_many :contributions
  has_many :finances, :through => :contributions
end
