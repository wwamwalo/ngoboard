class Bank < ActiveRecord::Base
  has_many :fortunes
  has_many :finances, :through => :fortunes
end
