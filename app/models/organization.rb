class Organization < ActiveRecord::Base
  mount_uploader :logo, AvatarUploader
  mount_uploader :constitution, AvatarUploader
  mount_uploader :minutes, AvatarUploader
  mount_uploader :budget, AvatarUploader
  mount_uploader :forward_letter, AvatarUploader
  mount_uploader :notarized_cert, AvatarUploader
  
  has_one :finance
  has_and_belongs_to_many :natures
  has_and_belongs_to_many :targets
  has_and_belongs_to_many :operations
  has_many :officials
  belongs_to :reservation
  
  validates :scope, presence:true  
end
