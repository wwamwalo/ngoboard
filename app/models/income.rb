class Income < ActiveRecord::Base
  has_many :donors
  has_many :finances, :through => :donor
end
