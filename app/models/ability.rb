class Ability
  include CanCan::Ability
  
  def initialize(admin)
    
    if admin.role == "User"
      can :read, Organization
      can :read, Official
      can :read, StaticPage      
    end
    
    if admin.role == "NGO" 
      can :read, StaticPage
      can [:create, :read, :update],[Reservation, Organization, Official, Finance, Govern, Operation]
      can [:create, :read, :update, :destroy],[Employee, Income, Target, Bank, Asset, Expense, Education, Nature]  
    end

    if admin.org_status == "OLD NGO"
      cannot [:create, :read, :update, :destroy],[Reservation]
    end
    
    if admin.role == "admin"
      can [:create, :read, :update, :destroy], [Admin, Organization, Reservation]
    end
    
    if admin.role == "superadmin"
      can :manage, :all
    end 

    if admin.role == "STUDENT" 
      can :manage, :all
    end

  end
end
