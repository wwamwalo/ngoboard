#type of organizations
class Collaborate < ActiveRecord::Base
  has_many :collaborations
  has_many :finances, :through => :collaborations
end
