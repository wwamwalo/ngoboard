class Asset < ActiveRecord::Base
  has_many :possessions
  has_many :finances, :through => :possessions
end
