class Finance < ActiveRecord::Base
  has_one :employee, :class_name=> "Employee"
  has_one :govern, :class_name=> "Govern"
  has_many :waivers, :class_name=> "Waiver"
  has_many :possessions
  has_many :assets, :through => :possessions, :class_name=> "Asset"
  has_many :donors
  has_many :incomes, :through => :donors, :class_name=> "Income"
  has_many :sectors
  has_many :expenses, :through => :sectors, :class_name=> "Sector"
  has_many :fortunes
  has_many :banks, :through => :fortunes, :class_name=> "Fortune"
  has_many :projects
  has_many :expenses, :through => :projects, :class_name=> "Project"
  has_many :benefits
  has_many :privileges, :through => :benefits, :class_name=> "Privilege"
  has_many :trained_members
  has_many :trainings, :through => :trained_members, :class_name=> "Training"
  has_many :locals
  has_many :local_contributions, :through => :locals, :class_name=> "LocalContribution"
  has_many :contributions
  has_many :government_contributions, :through => :contributions, :class_name=> "GovernmentContribution"
  has_many :collaborations
  has_many :collaborates, :through => :collaborations, :class_name=> "Collaborate"
  has_many :occurences
  has_many :elections, :through => :occurences, :class_name=> "Election"
  belongs_to :organization
  
  accepts_nested_attributes_for :possessions, :reject_if => :all_blank, :allow_destroy => true 
  accepts_nested_attributes_for :donors, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :sectors, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :fortunes, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :employee, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :projects, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :benefits, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :trained_members, :reject_if => :all_blank, :allow_destroy => true     
  accepts_nested_attributes_for :locals, :reject_if => :all_blank, :allow_destroy => true  
  accepts_nested_attributes_for :contributions, :reject_if => :all_blank, :allow_destroy => true  
  accepts_nested_attributes_for :waivers, :reject_if => :all_blank, :allow_destroy => true  
  accepts_nested_attributes_for :collaborations, :reject_if => :all_blank, :allow_destroy => true  
  accepts_nested_attributes_for :govern, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :occurences, :reject_if => :all_blank, :allow_destroy => true
end
