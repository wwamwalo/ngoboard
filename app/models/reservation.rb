class Reservation < ActiveRecord::Base
  validates :names, presence: true, length: {maximum: 50}
  #validates :address, presence: true, length: {maximum: 50}
  validates :name1, presence: true, length: {maximum: 50}
  validates :name2, presence: true, length: {maximum: 50}
  validates :name3, presence: true, length: {maximum: 50}
  validates :payment, presence: true
  
  has_one :organization
  belongs_to :admin
end
