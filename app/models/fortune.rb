class Fortune < ActiveRecord::Base
  belongs_to :finance
  belongs_to :bank
  
  accepts_nested_attributes_for :bank
end
