class Privilege < ActiveRecord::Base
  has_many :benefits
  has_many :finances, :through => :benefits
end
