class Sector < ActiveRecord::Base
  belongs_to :finance
  belongs_to :expense
  
  accepts_nested_attributes_for :expense
end
