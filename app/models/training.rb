class Training < ActiveRecord::Base
  has_many :trained_members
  has_many :finances, :through => :trained_members
end
