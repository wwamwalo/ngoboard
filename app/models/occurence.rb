class Occurence < ActiveRecord::Base
  belongs_to :finance
  belongs_to :election
  
  accepts_nested_attributes_for :election
end
