class TrainedMember < ActiveRecord::Base
  belongs_to :finance
  belongs_to :training
  
  accepts_nested_attributes_for :training
end
