class Expense < ActiveRecord::Base
  has_many :sectors
  has_many :finances, :through => :sector
  has_many :projects
  has_many :finances, :through => :projects
end
