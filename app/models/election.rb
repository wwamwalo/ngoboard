class Election < ActiveRecord::Base
  has_many :occurences
  has_many :finances, :through => :occurences
end
