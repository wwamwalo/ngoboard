class Official < ActiveRecord::Base
  mount_uploader :passport, AvatarUploader
  mount_uploader :id_upload, AvatarUploader
  validates :passport_number, presence: true, length: {maximum: 50}
  belongs_to :organization
  has_many :educations
  accepts_nested_attributes_for :educations,:allow_destroy => true #, :reject_if => :all_blank
end
