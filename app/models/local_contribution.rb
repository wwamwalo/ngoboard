class LocalContribution < ActiveRecord::Base
  has_many :locals
  has_many :finances, :through => :locals
end
