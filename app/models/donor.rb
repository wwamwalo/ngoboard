class Donor < ActiveRecord::Base
  belongs_to :finance
  belongs_to :income
  
  accepts_nested_attributes_for :income
end
