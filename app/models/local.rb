#contribution by local people
class Local < ActiveRecord::Base
  belongs_to :finance
  belongs_to :local_contribution
  
  accepts_nested_attributes_for :local_contribution
end
