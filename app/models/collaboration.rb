class Collaboration < ActiveRecord::Base
  belongs_to :finance
  belongs_to :collaborate
  
  accepts_nested_attributes_for :collaborate
end
