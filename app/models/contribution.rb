#government contribution
class Contribution < ActiveRecord::Base
  belongs_to :finance
  belongs_to :government_contribution
  
  accepts_nested_attributes_for :government_contribution
end
