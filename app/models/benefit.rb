class Benefit < ActiveRecord::Base
  belongs_to :finance
  belongs_to :privilege
  
  accepts_nested_attributes_for :privilege
end
