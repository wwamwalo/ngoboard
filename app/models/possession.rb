class Possession < ActiveRecord::Base
  belongs_to :finance
  belongs_to :asset
  
  accepts_nested_attributes_for :asset
end
