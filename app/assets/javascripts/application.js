// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require beyond/skins.min
//= require beyond/bootstrap.min
//= require beyond/slimscroll/jquery.slimscroll.min
//= require beyond/beyond
//= require beyond/fuelux/wizard/wizard-custom
//= require beyond/toastr/toastr
//= require beyond/charts/sparkline/jquery.sparkline
//= require beyond/charts/sparkline/sparkline-init
//= require beyond/charts/easypiechart/jquery.easypiechart
//= require beyond/charts/easypiechart/easypiechart-init
//= require best_in_place
//= require best_in_place.purr
//= require cocoon
//= require moment
//= require pickers
//= require beyond/bootbox/bootbox
//= require beyond/datetime/daterangepicker
