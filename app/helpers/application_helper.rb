module ApplicationHelper
  def full_title(page_title)
    base_title = "NC"
    if page_title.empty?
      base_title      
    else
      "#{base_title} | #{page_title}"
    end
  end
  
  def full_header(page_title)
    base_title = "Nevlah Capital- Taking financial innovation to the next level"
    if page_title.empty?
      base_title      
    else
      "#{page_title}"
    end
  end 
  
end
