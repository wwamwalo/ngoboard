crumb :root do
  link "Home", root_path
end

crumb :reservations do
   link "Reservations", reservations_path
 end

crumb :newreservation do 
   link 'new reservation', newreservation_path
   parent :reservations
end

crumb :reservation do |reservation|
   link reservation.name1, reservation_path(reservation)
   parent :reservations
 end

crumb :organizations do
   link "Organizations", organizations_path
 end

crumb :neworganization do 
   link 'new organization', neworganization_path
   parent :organizations
end

crumb :organization do |organization|
   link organization.email, reservation_path(organization)
   parent :organizations
 end

crumb :finances do
   link "finances", finances_path
 end

crumb :newfinance do 
   link 'new finance', newfinance_path
   parent :finances
end

crumb :finance do |finance|
   link "my finance submission", finance_path(finance)
   parent :finances
 end

crumb :profile do
   link "profile", edit_admin_registration_path
 end

crumb :officials do
   link "Officials", officials_path
 end

crumb :newofficial do 
   link 'new official', newofficial_path
   parent :reservations
end

crumb :official do |official|
   link official.name, official_path(official)
   parent :officials
 end


# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).