require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module Ngo
  class Application < Rails::Application
    config.assets.paths << Rails.root.join("vendor","assets", "fonts", "beyond") #custom font 
    config.action_mailer.default_url_options = { host: 'localhost:3000' }
    config.action_mailer.delivery_method = :smtp
    ActionMailer::Base.smtp_settings = {
      :address              => "smtp.gmail.com",
      :port                 => 587,
      :domain               => "gmail.com",
      :authentication       => "plain",
      :enable_starttls_auto => true,
      :user_name            => "wilyoj",
      :password             => "r6r5bb!!"
    }
  end
end
