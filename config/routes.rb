Ngo::Application.routes.draw do
  devise_for :admins
  resources :products, :reservations, :organizations, :officials, :educations, :targets, :operations, 
            :natures, :finances, :assets, :banks, :expenses, :incomes, :employees, :governs
  root to: "static_pages#dashboard"
  match '/newreservation', to: 'reservations#new', via: 'get'
  match '/neworganization', to: 'organizations#new', via: 'get'
  match '/newofficial', to: 'officials#new', via: 'get'
  match '/neweducation', to: 'educations#new', via: 'get'
  match '/dashboard', to: 'static_pages#dashboard', via: 'get'
  match '/register', to: 'static_pages#register', via: 'get'
  match '/error', to: 'static_pages#error', via: 'get'
  match '/newtarget', to: 'targets#new', via: 'get'
  match '/newnature', to: 'natures#new', via: 'get'
  match '/newoperation', to: 'operations#new', via: 'get'
  match '/newfinance', to: 'finances#new', via: 'get'
  match '/newasset', to: 'assets#new', via: 'get'
  match '/newbank', to: 'banks#new', via: 'get'
  match '/newexpense', to: 'expenses#new', via: 'get'
  match '/newincome', to: 'incomes#new', via: 'get'
  match '/newemployee', to: 'employees#new', via: 'get'
  match '/newgovern', to: 'governs#new', via: 'get'
end
